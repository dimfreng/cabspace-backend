
class Advertiser < ActiveRecord::Base
	has_many :campaigns
	

	include GladeGUI
	
	def show()
		load_glade(__FILE__)
		set_glade_all()
		show_window()
	end

	
		
	def to_s # this is what shows in the VR::ListView
		name
	end
	
	def on_delete_clicked(*args)
		self.delete		
		destroy_window()
	end

	def on_submit_clicked(*args)
		get_glade_all()
		save
		destroy_window()
	end

	

end
