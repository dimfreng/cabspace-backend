
class TimeSlot < ActiveRecord::Base
	has_one :campaign

	include GladeGUI

	def show()
		load_glade(__FILE__)
		set_glade_all()
		show_window()
	end	
	
	def to_s
		timeLimit.to_s
	end

	def on_delete_clicked(*args)
		self.delete		
		destroy_window()
	end

	def on_submit_clicked(*args)
		self.status = "vacant"
		get_glade_all()
		save
		destroy_window()
	end

end
