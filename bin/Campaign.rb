
class Campaign < ActiveRecord::Base
	belongs_to :advertiser
	belongs_to :time_slot

	include GladeGUI
	
	def show()
		load_glade(__FILE__)
		@view = VR::FileTreeView.new("./img")
		@view.refresh(ENV['HOME'] + '/Desktop')
    @builder["scrolledwindow2"].add(@view)
		@view.drag_to(@builder["image_path"])
    @view.drag_to(@builder["logo_path"])
    @view.drag_to(@builder["video_path"])
		set_glade_all()
		show_window()
	end

	def add_show(advertiser,time)
		load_glade(__FILE__)
		@view = VR::FileTreeView.new("./img")
		@view.refresh(ENV['HOME'] + '/Desktop')
    @builder["scrolledwindow2"].add(@view)
		@view.drag_to(@builder["image_path"])
    @view.drag_to(@builder["logo_path"])
    @view.drag_to(@builder["video_path"])
		@builder["advertiser_id"].text = advertiser.to_s
		@builder["time_slot_id"].text = time.to_s
		show_window()
	end

	def image_path__drag_drop(*args)
		path = @builder["image_path"]
		if path.dragged_widget.is_a?(Gtk::TextView)
			path.text = path.dragged_widget.buffer.text.slice(0,15) 
		elsif path.dragged_widget.is_a?(VR::FileTreeView)
			path.text = @view.get_selected_path()
		end
	end

	def logo_path__drag_drop(*args)
		path = @builder["logo_path"]
		if path.dragged_widget.is_a?(Gtk::TextView)
			path.text = path.dragged_widget.buffer.text.slice(0,15) 
		elsif path.dragged_widget.is_a?(VR::FileTreeView)
			path.text = @view.get_selected_path()
		end
	end

	def video_path__drag_drop(*args)
		path = @builder["video_path"]
		if path.dragged_widget.is_a?(Gtk::TextView)
			path.text = path.dragged_widget.buffer.text.slice(0,15) 
		elsif path.dragged_widget.is_a?(VR::FileTreeView)
			path.text = @view.get_selected_path()
		end
	end

	def to_s # this is what shows in the VR::ListView
		name
	end

	def on_delete_clicked(*args)
		self.delete		
		destroy_window()
	end
	

	def on_campaign_type_toggled(args)
		result = args.active? ? false : true	
		 @builder["display_type"].set_sensitive(result)
		 @builder["label7"].set_sensitive(result)
		 @builder["label10"].set_sensitive(result)
		 @builder["video_path"].set_sensitive(result)
  end
	
	def on_submit_clicked(*args)

		if @builder["advertiser_id"].text != "" && @builder["time_slot_id"].text != ""
			timeSlot = TimeSlot.find(@builder["time_slot_id"].text)
			timeSlot.status = "occupied"
			timeSlot.save
			self.status = "active"
			get_glade_all()
			save
			destroy_window()
			
		end
	end

end
