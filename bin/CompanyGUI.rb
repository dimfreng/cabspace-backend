
class CompanyGUI

	include GladeGUI
	require 'rubygems'
	require 'zip/zip'
  require 'find'


	def before_show()
		@employer_view = VR::ListView.new(:id => Integer, :advertiser => Advertiser,  :address => String, :company_type => String)
  	@builder["scroll_employer"].add(@employer_view)
		@employer_view.selection.signal_connect("changed") { employer_view__changed }
		@employer_view.col_width(:id => 100,:advertiser => 160, :address => 240, :company_type => 180)

  	@employee_view = VR::ListView.new(:campaign => Campaign, :advertiser => Advertiser ,:expiration_date => Date, :status => String)
  	@builder["scroll_employee"].add(@employee_view)
		@employee_view.col_width(:campaign => 200, :advertiser => 160 ,:expiration_date => 150, :status => 50 )

  	@paycheck_view = VR::ListView.new(:id => Integer , :timeLimit => TimeSlot , :status => String)
  	@builder["scroll_paycheck"].add(@paycheck_view)
		@paycheck_view.col_width(:id => 100,:timeLimit => 100, :status => 200)

		Advertiser.all.each do |e|
			row = @employer_view.add_row
			row[:advertiser] = e
			row.load_object(e)
		end
		
		timerefresh
	end
	

	def	timerefresh
		@paycheck_view.model.clear
		TimeSlot.where("status != ?", "occupied").each do |e|
			row = @paycheck_view.add_row
			row.load_object(e) #this loads employee object
			row[:timeLimit] = e
		end
	end



	def employer_view__changed
		return unless row = @employer_view.selected_rows[0]
		@employee_view.model.clear
		Campaign.where(:advertiser_id => row[:advertiser].id).each do |e|
			row = @employee_view.add_row
			row[:campaign] = e
			row.load_object(e)
		end

	end


		


	def on_refresh_clicked(*args)
		Campaign.where("status != ?", "expired").each do |campaign|
			if campaign.expiration_date < DateTime.now
				campaign.status = "expired"	
				campaign.save		
			end
		end
		
		timerefresh
		employer_view__changed
		@builder["notice"].label = ""
	end
	
	def on_advertiser_clicked(*args)	
		Advertiser.new.show
	end
	
	def on_campaign_clicked(*args)
		row = @employer_view.selected_rows[0]
    row2 = @paycheck_view.selected_rows[0]	
		if row2 != nil && row != nil
		 
		 Campaign.new.add_show(row[:advertiser].id, row2[:timeLimit].id)
		end
		
	end

	def zip(dir, zip_dir, remove_after = false)
    Zip::ZipFile.open(zip_dir, Zip::ZipFile::CREATE)do |zipfile|
      Find.find(dir) do |path|
        Find.prune if File.basename(path)[0] == ?.
        dest = /#{dir}\/(\w.*)/.match(path)
        # Skip files if they exists
        begin
          zipfile.add(dest[1],path) if dest
        rescue Zip::ZipEntryExistsError
        end
      end
    end
    FileUtils.rm_rf(dir) if remove_after
  end

	def on_export_clicked(*args)
		require 'fileutils'
		FileUtils.cd(ENV['HOME'] + '/Desktop') do 
			FileUtils.rm_rf('cabspace')
			FileUtils.mkdir_p("cabspace")
			file = File.new('file.json', 'w+')
			FileUtils.move(file ,'cabspace')
				file.write("[\n")
			campaigns = Campaign.where("status != ?", "expired")
			campaigns.each do |campaign|
				
				file.write("{ \"name\":\""+campaign.name+"\",\n \"description\":\""+campaign.description+"\",\n \"expiration\":\""+(campaign.expiration_date).to_s+
										"\",\n \"image\":\""+campaign.image_path+"\",\n \"logo\":\""+campaign.logo_path+"\",\n \"video\":\""+campaign.video_path+
										"\",\n \"duration\":"+(campaign.time_slot.timeLimit).to_s+",\n \"type\":\""+self.is_video(campaign.display_type)+
                    "\",\n \"campaignType\":\""+self.is_minor(campaign.campaign_type)+"\"\n  }")
				if campaign != campaigns.last
			    file.write(",\n")
			  end
				if campaign.image_path != ""  
	    	 FileUtils.copy(campaign.image_path, 'cabspace')
				end
				if campaign.logo_path != ""
				 FileUtils.copy(campaign.logo_path, 'cabspace')
				end
				if campaign.video_path != ""
				 FileUtils.copy(campaign.video_path, 'cabspace')
				end		
			end
				file.write("\n]")
				file.close
    end	
			self.zip(ENV['HOME'] + '/Desktop/cabspace', ENV['HOME'] + '/Desktop/cabspace.rar',true)
			@builder["notice"].label = "cabspace forlder has been created. \n location: "+ENV['HOME'] + '/Desktop'		
  end

	def is_video(args)
		return(args ? "video":"image" )
  end
	
	def is_minor(args)
		return(args ? "minor":"major" )
  end
	
	def on_timeslot_clicked(*args)
		TimeSlot.new.show
	end


end
